.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../../definitions.rst

.. _SupportedBoards:

Supported Boards
################

This section details the boards supported as part of |main_project_name|.

.. toctree::
   :maxdepth: 1

   96b-Avenger
   96b-nitrogen
   seco-intel-b68
   seco-imx8mm-c61
   raspberrypi4
   arduino-nano-33-ble
   nRF52840-DK
